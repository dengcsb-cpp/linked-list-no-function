#include <iostream>
#include <string>

using namespace std;

struct Node
{
	int data = 0; // The actual value/object
	Node* next = nullptr; // Pointer to the next node
};

int main()
{
	// Creating the linked list (3 nodes)
	// =======================================
	
	// Track these 3 pointers while building a linked list
	Node* head = nullptr;
	Node* current = nullptr;
	Node* previous = nullptr;

	// Create the 1st node. We'll use the 'current' pointer
	current = new Node;
	current->data = 10;
	// The first node is always the 'head' node.
	// The head node represents the entire linked list
	head = current;
	// After creating the node, set this as the previous node
	// We'll use this to setup the 'tail' later
	previous = current;

	// Create the 2nd node. Just reuse the 'current' pointer
	current = new Node;
	current->data = 20;
	// Link the first node's tail to this node
	previous->next = current;
	// Now we're done, this will be the new 'previous' node
	previous = current;

	// Create the 3rd node. We'll just repeat what we did earlier
	current = new Node;
	current->data = 30;
	previous->next = current;
	// Since this is the end of the list,
	// Its tail should be set to null
	current->next = nullptr;


	// Printing the linked list
	// ========================================
	// We only need the head node to make this work
	
	// 1. Store the head node in a different pointer
	Node* current1 = head;

	// 2. Traverse the nodes until we reach null
	// We do this by moving the current node each loop
	while (current1 != nullptr)
	{
		cout << current1->data << endl;
		// Move the current pointer to the next node
		current1 = current1->next;
	}


	// Adding a new node (4th node) at the end
	// ========================================

	// Create the node to be added
	Node* toAdd = new Node;
	toAdd->data = 40;

	// Find the last node. We'll put the new node after it.
	Node* last = nullptr;
	// This requires traversing the entire list. Start from the head.
	Node* current2 = head;

	while (current2 != nullptr)
	{
		last = current2;
		current2 = current2->next;
	}

	// The last node's tail should point to the new node
	last->next = toAdd;
	// The new node should point to null
	toAdd->next = nullptr;


	// Removing a node (index 2)
	// ===========================================
	int indexToRemove = 2;

	// We have to count from the head node until we reach the target node
	Node* toDelete = head;
	// Keep track of the 'previous' node so we can reconnect the gaps
	Node* previous1 = nullptr;

	// Traverse the nodes. 
	// We'll use a for-loop since we're counting from index 0.
	for (int i = 0; i < indexToRemove; i++)
	{
		// Keep track of the previous node
		previous1 = toDelete;
		// Move the pointer to the next node
		toDelete = toDelete->next;
	}

	// Before we delete the node, we have to reconnect the link first
	// so that index1 now connects to index3
	// Note that previous is index 1 and toDelete->next is index 3
	previous1->next = toDelete->next;
	
	// Delete the node
	delete toDelete;
	toDelete = nullptr;

	system("pause");
	return 0;
}
